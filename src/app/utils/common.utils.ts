export function isDefined(arg: any): boolean {
  return arg !== null && typeof arg !== 'undefined';
}

export function isNotDefined(arg: any): boolean {
  return !isDefined(arg);
}

export function isEmpty(val: string | number | boolean) {
  return isNotDefined(val) || (typeof val === 'string' && val.length === 0);
}

export function interpolate(template: string, values: object) {
  return template.replace(/#{([\w0-9]+)}/g, (val, match) => {
    return isEmpty(values[match]) ? val : values[match];
  });
}

export function assign<T>(target: T): T;
export function assign<T, U>(target: T, source: U): T & U;
export function assign<T, U, V>(target: T, source1: U, source2: V): T & U & V;
export function assign<T, U, V, W>(target: T, source1: U, source2: V, source3: W): T & U & V & W;
export function assign(target: object, ...sources: any[]): any;
export function assign(...inputs: any[]): any {
  const [ target, ...sources] = inputs;
  return Object.assign({}, target, ...sources);
}
