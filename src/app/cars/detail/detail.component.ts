import { ChangeDetectorRef, Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { of, Subscription } from 'rxjs';
import { catchError, first, map, mergeMap, tap } from 'rxjs/operators';

import { Car } from './../cars.model';
import { CarsService } from './../cars.service';

@Component({
  selector: 'app-cars-detail',
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: true
})
export class DetailComponent implements OnDestroy {

  i18NPrefix = 'cars.detail.';

  car: Car;
  loading = false;
  loadingError = false;
  editing = false;
  deleting = false;
  deleteError = false;

  private routeSub: Subscription;

  constructor(
    private carsService: CarsService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private cd: ChangeDetectorRef
  ) {
    this.routeSub = this.route.paramMap.pipe(tap(() => {
      this.editing = false;
      this.car = null;
      this.loading = true;
      this.loadingError = false;
      this.cd.markForCheck();
    }), map((paramMap) => paramMap.get('carId')), mergeMap((id) => {
      return this.carsService.getCar$(id).pipe(catchError((e) => {
        console.error(e);
        return of(null);
      }));
    })).subscribe((car) => {
      this.car = car;
      this.carsService.selectCar(car);
      this.loading = false;
      this.loadingError = !this.car;
      this.cd.markForCheck();
    });
  }

  ngOnDestroy() {
    this.routeSub && this.routeSub.unsubscribe();
    this.carsService.selectCar(null);
  }

  onClose(): void {
    this.router.navigate(['../..'], { relativeTo: this.route });
  }

  onEdit(): void {
    this.editing = true;
  }

  onEditClose(): void {
    this.editing = false;
  }

  onDelete(): void {
    this.translate.get(this.i18NPrefix + 'deleteConfirmation', {
      name: `${this.car.brand} ${this.car.model}`
    }).pipe(first()).subscribe((confirmMsg) => {
      if (confirm(confirmMsg)) {
        this.deleting = true;
        this.deleteError = false;

        this.carsService.deleteCar$(this.car.id).subscribe(() => {
          this.deleting = false;
          this.onClose();
        }, () => {
          this.deleteError = true;
          this.deleting = false;
        });
      }
    });
  }

}
