import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { NgForm } from '@angular/forms';

import { assign } from '../../utils/common.utils';
import { trimToNull } from '../../validation/validation.utils';
import { CarsService } from '../cars.service';
import { Car } from './../cars.model';

@Component({
  selector: 'app-cars-template-form',
  templateUrl: './template-form.component.html',
  styleUrls: ['./template-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: true
})
export class TemplateFormComponent implements OnInit {

  i18NPrefix = 'cars.form.';
  detailI18NPrefix = 'cars.detail.';

  @Input() car?: Car;
  @Output() closeForm = new EventEmitter<any>();
  @Output() carCreate = new EventEmitter<Car>();
  @Output() carUpdate = new EventEmitter<Car>();

  model: Car;
  alreadySubmitted = false;
  submitting = false;
  submitError = false;

  private isCreate: boolean;

  constructor(
    private carsService: CarsService,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.isCreate = !this.car;
    if (!this.car) {
      this.car = {
        id: null,
        brand: null,
        model: null,
        fuel: 'diesel',
        power: null,
        description: null
      };
    }
    this.model = Object.assign({}, this.car);
  }

  onClose(): void {
    this.closeForm.emit(null);
  }

  onSubmit(form: NgForm): void {
    this.alreadySubmitted = true;
    if (!form.valid || this.submitting) {
      return;
    }
    this.submitting = true;
    this.submitError = false;

    const value: Car = assign(this.model);
    value.brand = trimToNull(value.brand);
    value.model = trimToNull(value.model);
    value.power = parseInt(trimToNull(value.power as any), 10);
    value.description = trimToNull(value.description);

    const obs = this.isCreate ? this.carsService.createCar$(value) : this.carsService.updateCar$(value);
    obs.subscribe((car) => {
      this.submitting = false;
      this.isCreate ? this.carCreate.emit(car) : this.carUpdate.emit(car);
      this.cd.markForCheck();
    }, (err) => {
      this.submitError = true;
      this.submitting = false;
      console.error(err);
      this.cd.markForCheck();
    });
  }

}
