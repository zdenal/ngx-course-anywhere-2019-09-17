import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Car } from '../cars.model';

@Component({
  selector: 'app-cars-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CreateComponent implements OnInit {

  i18NPrefix = 'cars.create.';

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
  }

  onClose(): void {
    this.router.navigate(['..'], { relativeTo: this.route });
  }

  onCarCreate(car: Car): void {
    this.router.navigate(['..', 'detail', car.id], { relativeTo: this.route });
  }

}
