import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { Cars, Car } from './cars.model';

@Injectable()
export abstract class CarsApiService {

  abstract getCars$(): Observable<Cars>;

  abstract getCar$(id: string): Observable<Car>;

  abstract updateCar$(car: Car): Observable<Car>;

  abstract createCar$(car: Car): Observable<Car>;

  abstract deleteCar$(id: string): Observable<any>;

}
