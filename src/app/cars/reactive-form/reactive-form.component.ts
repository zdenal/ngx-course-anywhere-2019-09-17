import { ChangeDetectorRef, Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { UniversalValidators } from 'ngx-validators';
import { notBlankValidator } from 'src/app/validation/validation.validators';

import { trimToNull } from '../../validation/validation.utils';
import { CarsService } from '../cars.service';
import { Car } from './../cars.model';

@Component({
  selector: 'app-cars-reactive-form',
  templateUrl: './reactive-form.component.html',
  styleUrls: ['./reactive-form.component.scss'],
  encapsulation: ViewEncapsulation.None,
  preserveWhitespaces: true
})
export class ReactiveFormComponent implements OnInit {

  i18NPrefix = 'cars.form.';
  detailI18NPrefix = 'cars.detail.';

  @Input() car?: Car;
  @Output() closeForm = new EventEmitter<any>();
  @Output() carCreate = new EventEmitter<Car>();
  @Output() carUpdate = new EventEmitter<Car>();

  form: FormGroup;
  alreadySubmitted = false;
  submitting = false;
  submitError = false;

  private isCreate: boolean;

  constructor(
    private carsService: CarsService,
    private fb: FormBuilder,
    private cd: ChangeDetectorRef
  ) { }

  ngOnInit() {
    this.isCreate = !this.car;
    if (!this.car) {
      this.car = {
        id: null,
        brand: null,
        model: null,
        fuel: 'diesel',
        power: null,
        description: null
      };
    }
    this.buildForm();
  }

  onClose(): void {
    this.closeForm.emit(null);
  }

  onSubmit(): void {
    this.alreadySubmitted = true;
    if (!this.form.valid || this.submitting) {
      return;
    }
    this.submitting = true;
    this.submitError = false;

    const value: Car = this.form.value;
    value.id = this.car.id;
    value.brand = trimToNull(value.brand);
    value.model = trimToNull(value.model);
    value.power = parseInt(trimToNull(value.power as any), 10);
    value.description = trimToNull(value.description);

    const obs = this.isCreate ? this.carsService.createCar$(value) : this.carsService.updateCar$(value);
    obs.subscribe((car) => {
      this.submitting = false;
      this.isCreate ? this.carCreate.emit(car) : this.carUpdate.emit(car);
      this.cd.markForCheck();
    }, (err) => {
      this.submitError = true;
      this.submitting = false;
      this.cd.markForCheck();
      console.error(err);
    });
  }

  private buildForm(): void {
    this.form = this.fb.group({
      brand: [this.car.brand, notBlankValidator],
      model: [this.car.model, notBlankValidator],
      fuel: [this.car.fuel, notBlankValidator],
      power: [this.car.power, [notBlankValidator, UniversalValidators.isInRange(1, 1000)]],
      description: [this.car.description]
    });
  }

}
