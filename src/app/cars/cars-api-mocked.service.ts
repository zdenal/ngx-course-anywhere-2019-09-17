import { Injectable } from '@angular/core';
import { Observable, timer } from 'rxjs';
import { map } from 'rxjs/operators';

import { cars } from './../../mocks/cars';
import { CarsApiService } from './cars-api.service';
import { Car, Cars } from './cars.model';

const delay = 1000;

@Injectable()
export class CarsApiMockedService extends CarsApiService {

  getCars$(): Observable<Cars> {
    return timer(delay).pipe(map(() => cars));
  }

  getCar$(id: string): Observable<Car> {
    return timer(delay).pipe(map(() => {
      const car = cars.find((c) => c.id === id);
      if (!car) {
        throw new Error(`Car with id ${id} not exists.`);
      }
      return car;
    }));
  }

  updateCar$(car: Car): Observable<Car> {
    return timer(delay).pipe(map(() => {
      const idx = cars.findIndex((c) => c.id === car.id);
      if (idx === -1) {
        throw new Error(`Car with id ${car.id} not exists.`);
      }
      cars[idx] = car;
      return car;
    }));
  }

  createCar$(car: Car): Observable<Car> {
    return timer(delay).pipe(map(() => {
      car.id = `${new Date().getTime()}`;
      cars.push(car);
      return car;
    }));
  }

  deleteCar$(id: string): Observable<any> {
    return timer(delay).pipe(map(() => {
      const idx = cars.findIndex((c) => c.id === id);
      if (idx === -1) {
        throw new Error(`Car with id ${id} not exists.`);
      }
      cars.splice(idx, 1);
      return null;
    }));
  }

}
