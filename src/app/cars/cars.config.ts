import { environment } from '../../environments/environment';

export class CarsConfig {
  getCarsUrl = `${environment.firebaseUriPrefix}/cars.json`;
  createCarUrl = `${environment.firebaseUriPrefix}/cars.json`;
  getCarUrl = `${environment.firebaseUriPrefix}/cars/#{id}.json`;
  updateCarUrl = `${environment.firebaseUriPrefix}/cars/#{id}.json`;
  deleteCarUrl = `${environment.firebaseUriPrefix}/cars/#{id}.json`;
}
